#pragma once
#include <Ball.h>

using namespace DirectX;

class BrickTile
{
public:
    virtual ~BrickTile() = default;
    BrickTile() = default;

    BrickTile(const RECT& Rect, XMVECTORF32 Color) : Rect(Rect), MColor(Color)
    {
        V1 = VertexPositionColor(SimpleMath::Vector2(Rect.left, Rect.top), MColor);
        V2 = VertexPositionColor(SimpleMath::Vector2(Rect.right, Rect.top), MColor);
        V3 = VertexPositionColor(SimpleMath::Vector2(Rect.right, Rect.bottom), MColor);
        V4 = VertexPositionColor(SimpleMath::Vector2(Rect.left, Rect.bottom), MColor);
        IsDestroyed = false;
    }

    void ExecuteBallCollision(Ball& Ball);
    bool CheckBallCollision(const Ball& Ball);

    VertexPositionColor V1;
    VertexPositionColor V2;
    VertexPositionColor V3;
    VertexPositionColor V4;
    bool IsDestroyed = false;
    Vec2 GetCenter() const;

protected:
    virtual bool IsOverlappingWith(const RECT& Other);

private:
    RECT Rect;
    XMVECTORF32 MColor;
};
