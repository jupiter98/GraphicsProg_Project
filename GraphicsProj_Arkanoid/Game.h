//
// Game.h
//

#pragma once

#include "DeviceResources.h"
#include "StepTimer.h"
#include "AnimatedTexture.h"
#include "BrickManager.h"
#include <Paddle.h>

class Ball;
// A basic game implementation that creates a D3D11 device and
// provides a game loop.
class Game final : public DX::IDeviceNotify
{
public:
    Game() noexcept(false);
    ~Game();

    Game& operator=(Game&&) = default;

    Game(Game const&) = delete;
    Game& operator=(Game const&) = delete;

    // Initialization and management
    void Initialize(HWND Window, int Width, int Height);

    // Basic game loop
    void Tick();

    // IDeviceNotify
    void OnDeviceLost() override;
    void OnDeviceRestored() override;

    // Messages
    static void OnActivated();
    void OnDeactivated();
    void OnSuspending();
    void OnResuming();
    void OnWindowMoved();
    void OnWindowSizeChanged(int Width, int Height);

    // Properties
    auto GetDefaultSize(int& Width, int& Height) const noexcept -> void;
    void OnNewAudioDevice() noexcept { MRetryAudio = true; }

private:
    void Update(DX::StepTimer const& Timer);
    void Render();

    void Clear();

    void CreateDeviceDependentResources();
    void CreateWindowSizeDependentResources();

    // Device resources.
    std::unique_ptr<DX::DeviceResources> MDeviceResources;

    // Rendering loop timer.
    DX::StepTimer MTimer;

    DirectX::SimpleMath::Vector2 MScreenPos;
    DirectX::SimpleMath::Vector2 MOrigin;
    std::unique_ptr<DirectX::CommonStates> MStates;
    std::unique_ptr<DirectX::BasicEffect> MEffect;

    RECT MTileRect;
    RECT MStretchRect;
    RECT MFullscreenRect;
    Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> MBackground;

    Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> MBallTexture;
    std::unique_ptr<DirectX::SpriteBatch> MSpriteBatch;

    using VertexType = DirectX::VertexPositionColor;
    std::unique_ptr<DirectX::PrimitiveBatch<VertexType>> MBatch;
    Microsoft::WRL::ComPtr<ID3D11InputLayout> MInputLayout;

    std::unique_ptr<DirectX::SpriteFont> MFont;
    DirectX::SimpleMath::Vector2 MFontPos;

    std::unique_ptr<DirectX::Keyboard> MKeyboard;
    std::unique_ptr<DirectX::Mouse> MMouse;

    Ball* MyBall;
    std::unique_ptr<BrickManager> Manager;
    Paddle* MyPaddle;
    RECT Walls;

    //game settings parameters
    static constexpr int BrickWidth = 40;
    static constexpr int BrickHeight = 40;
    static constexpr int NBricksAcross = 10;
    static constexpr int NBricksDown = 3;

    bool GameOver = false;

    std::unique_ptr<DirectX::AudioEngine> MAudEngine;
    bool MRetryAudio;

    std::unique_ptr<DirectX::SoundEffect> MPaddleHitSound;
    std::unique_ptr<DirectX::SoundEffect> MGameOverSound;
    std::unique_ptr<DirectX::SoundEffect> MBrickExplSound;
};
