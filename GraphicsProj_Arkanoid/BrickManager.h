#pragma once
#include "Ball.h"
#include <BrickTile.h>
using GeometryBatch = std::unique_ptr<PrimitiveBatch<VertexPositionColor>>;

class BrickManager
{
public:
    void CreateBricks(int nBricksAcross, int nBricksDown, int brickWidth, int BrickHeight);
    void UpdateBrickState(std::vector<BrickTile>& BrickList, Ball& Ball, const GeometryBatch& Batch);
    void RemoveAllBricks();
    std::vector<BrickTile> BrickList;
    std::unique_ptr<SoundEffect> BrickSound;
    explicit BrickManager(std::unique_ptr<SoundEffect> InSound);

    void SetBrickSpawnOffset(const int InLeftOffset, const int InTopOffset)
    {
        LeftOffset = InLeftOffset;
        TopOffset = InTopOffset;
    }

private:
    int BrickDistance = 5;
    int LeftOffset = 200;
    int TopOffset = 100;
    int DestroyedBricks = 0;
};
