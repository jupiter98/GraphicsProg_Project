#include "pch.h"
#include "BrickTile.h"
#include <cassert>

void BrickTile::ExecuteBallCollision(Ball& Ball)
{
    assert(CheckBallCollision(Ball));

    const Vec2 BallPos = Ball.GetPosition();

    if (std::signbit(Ball.GetVel().x) == std::signbit((BallPos - GetCenter()).x))
    {
        Ball.ReboundY();
    }
    else if (BallPos.x >= Rect.left && BallPos.x <= Rect.right)
    {
        Ball.ReboundY();
    }
    else
    {
        Ball.ReboundX();
    }
    IsDestroyed = true;
}

bool BrickTile::CheckBallCollision(const Ball& Ball)
{
    return !IsDestroyed && IsOverlappingWith(Ball.GetRect());
}

bool BrickTile::IsOverlappingWith(const RECT& Other)
{
    return Rect.right > Other.left && Rect.left < Other.right
        && Rect.bottom > Other.top && Rect.top < Other.bottom;
}

Vec2 BrickTile::GetCenter() const
{
    return Vec2((Rect.left + Rect.right) / 2.0f, (Rect.top + Rect.bottom) / 2.0f);
}
