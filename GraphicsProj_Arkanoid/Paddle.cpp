#include "pch.h"
#include "Paddle.h"


Paddle::Paddle(const Vec2 Pos, const float HalfWidth, const float HalfHeight) : HalfWidth(HalfWidth),
    HalfHeight(HalfHeight), Pos(Pos), V1(),
    V2(),
    V3(),
    V4()
{
}

bool Paddle::DoBallCollision(Ball& Ball)
{
    if (IsOverlappingWith(Ball.GetRect()))
    {
        const Vec2 BallPos = Ball.GetPosition();

        if (std::signbit(Ball.GetVel().x) == std::signbit((BallPos - Pos).x) || (BallPos.x >= GetRect().left && BallPos.
            x <= GetRect().right))
        {
            const float XDiff = BallPos.x - Pos.x;
            const Vec2 Dir(XDiff * ExitXValue, -1.0f);
            Ball.SetDirection(Dir);
        }
        else
        {
            Ball.ReboundX();
        }
        return true;
    }
    return false;
}

void Paddle::DoWallCollision(const RECT& Walls)
{
    const RECT PaddleRect = GetRect();
    if (PaddleRect.left < Walls.left)
    {
        Pos.x += Walls.left - PaddleRect.left;
    }
    if (PaddleRect.right > Walls.right)
    {
        Pos.x -= PaddleRect.right - Walls.right;
    }
}

void Paddle::Update(const Keyboard::State& Key, const float DeltaTime)
{
    if (Key.Left)
    {
        Pos.x -= Speed * DeltaTime;
    }
    if (Key.Right)
    {
        Pos.x += Speed * DeltaTime;
    }
}

RECT Paddle::GetRect() const
{
    const Vec2 Half(HalfWidth, HalfHeight);
    RECT PaddleRect;
    PaddleRect.left = Pos.x - Half.x;
    PaddleRect.top = Pos.y - Half.y;
    PaddleRect.right = Pos.x + Half.x;
    PaddleRect.bottom = Pos.y + Half.y;
    return PaddleRect;
}

void Paddle::Draw(const GeometryBatch& Batch)
{
    RECT CurrentPaddleRect = GetRect();
    V1 = VertexPositionColor(SimpleMath::Vector2(CurrentPaddleRect.left, CurrentPaddleRect.top), Color);
    V2 = VertexPositionColor(SimpleMath::Vector2(CurrentPaddleRect.right,CurrentPaddleRect.top), Color);
    V3 = VertexPositionColor(SimpleMath::Vector2(CurrentPaddleRect.right,CurrentPaddleRect.bottom), Color);
    V4 = VertexPositionColor(SimpleMath::Vector2(CurrentPaddleRect.left, CurrentPaddleRect.bottom), Color);

    Batch->DrawQuad(V1, V2, V3, V4);
}

bool Paddle::IsOverlappingWith(const RECT& Rect)
{
    return GetRect().right > Rect.left && GetRect().left < Rect.right
        && GetRect().bottom > Rect.top && GetRect().top < Rect.bottom;
}
