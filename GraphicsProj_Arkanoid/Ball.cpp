#include "pch.h"
#include "Ball.h"

Ball::Ball(const Vec2& Pos, const Vec2& Dir) : Pos(Pos)
{
    SetDirection(Dir);
}

void Ball::Update(const float DeltaTime)
{
    Pos += Vel * DeltaTime;
}

Vec2 Ball::GetPosition() const
{
    return Pos;
}

int Ball::DoWallCollision(const RECT& Walls)
{
    int Collided = 0;
    const RECT Rect = GetRect();
    if (Rect.left < Walls.left)
    {
        Pos.x += Walls.left - Rect.left;
        ReboundX();
        Collided = 1;
    }
    else if (Rect.right > Walls.right)
    {
        Pos.x -= Rect.right - Walls.right;
        ReboundX();
        Collided = 1;
    }
    if (Rect.top < Walls.top)
    {
        Pos.y += Walls.top - Rect.top;

        ReboundY();
        Collided = 1;
    }
    else if (Rect.bottom > Walls.bottom)
    {
        Pos.y -= Rect.bottom - Walls.bottom;

        ReboundY();
        Collided = 2;
    }

    return Collided;
}

void Ball::ReboundX()
{
    Vel.x = -Vel.x;
}

void Ball::ReboundY()
{
    Vel.y = -Vel.y;
}

RECT Ball::GetRect() const
{
    constexpr Vec2 Half(Radius, Radius);
    RECT BallRect;
    BallRect.left = Pos.x - Half.x;
    BallRect.top = Pos.y - Half.y;
    BallRect.right = Pos.x + Half.x;
    BallRect.bottom = Pos.y + Half.y;
    return BallRect;
}

Vec2 Ball::GetVel() const
{
    return Vel;
}

void Ball::SetDirection(const Vec2& Dir)
{
    Vec2 DirCpy = Dir;
    DirCpy.Normalize();
    Vel = DirCpy * BallSpeed;
}
