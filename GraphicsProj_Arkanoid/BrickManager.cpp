#include "pch.h"
#include "BrickManager.h"
#include "BrickTile.h"

void BrickManager::CreateBricks(int nBricksAcross, int nBricksDown, int brickWidth, int brickHeight)
{
    const float SpawnOffset = brickWidth + BrickDistance;
    const float StartX = LeftOffset;
    const float StartY = TopOffset;

    for (int y = 0; y < nBricksDown; ++y)
    {
        for (int x = 0; x < nBricksAcross; ++x)
        {
            RECT BrickRect;
            BrickRect.left = static_cast<LONG>(StartX + SpawnOffset * x);
            BrickRect.top = static_cast<LONG>(StartY + SpawnOffset * y);
            BrickRect.right = BrickRect.left + brickWidth;
            BrickRect.bottom = BrickRect.top + brickHeight;

            const XMVECTORF32 Color = (y % 2 == 0) ? Colors::Orange : Colors::OrangeRed;

            auto Brick = BrickTile(BrickRect, Color);
            BrickList.push_back(Brick);
        }
    }
}

void BrickManager::UpdateBrickState(std::vector<BrickTile>& BrickList, Ball& Ball, const GeometryBatch& Batch)
{
    int BrickIndex = 0;
    bool HasCollided = false;
    float CurrentCollDist = 0;
    int CurColIndex = 0;
    for (auto BrickElem = BrickList.begin(); BrickElem != BrickList.end(); ++BrickElem)
    {
        if (!BrickElem->IsDestroyed)
        {
            Batch->DrawQuad(BrickElem->V1, BrickElem->V2, BrickElem->V3, BrickElem->V4);
        }
        if (BrickElem->CheckBallCollision(Ball))
        {
            const float newCollDistSquared = (Ball.GetPosition() - BrickElem->GetCenter()).LengthSquared();
            if (HasCollided)
            {
                if (newCollDistSquared < CurrentCollDist)
                {
                    CurrentCollDist = newCollDistSquared;
                    CurColIndex = BrickIndex;
                }
            }
            else
            {
                CurrentCollDist = newCollDistSquared;
                CurColIndex = BrickIndex;
                HasCollided = true;
            }

            DestroyedBricks++;

            if (BrickSound && !BrickSound->IsInUse())
            {
                BrickSound->Play();
            }
        }

        BrickIndex++;
    }
    if (HasCollided)
    {
        BrickList[CurColIndex].ExecuteBallCollision(Ball);
    }
    if (DestroyedBricks == BrickList.size())
    {
        //handle restart
    }
}

void BrickManager::RemoveAllBricks()
{
    BrickList.clear();
}

BrickManager::BrickManager(std::unique_ptr<DirectX::SoundEffect> InSound)
{
    BrickSound = std::move(InSound);
}
