#pragma once
#include <SimpleMath.h>
using Vec2 = DirectX::SimpleMath::Vector2;

class Ball
{
public:
    Ball(const Vec2& Pos, const Vec2& Dir);
    void Update(float DeltaTime);
    Vec2 GetPosition() const;
    int DoWallCollision(const RECT& Walls);
    void ReboundX();
    void ReboundY();
    RECT GetRect() const;
    Vec2 GetVel() const;
    void SetDirection(const Vec2& Dir);

private:
    static constexpr float Radius = 10.0f;
    Vec2 Pos;
    Vec2 Vel;
    float BallSpeed = 400.0f;
};
