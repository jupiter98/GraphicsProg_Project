#pragma once
#include "Ball.h"
#include "BrickTile.h"

using GeometryBatch = std::unique_ptr<DirectX::PrimitiveBatch<DirectX::VertexPositionColor>>;

class Paddle : public BrickTile
{
public:
    Paddle(Vec2 Pos, float HalfWidth, float HalfHeight);
    bool DoBallCollision(Ball& Ball);
    void DoWallCollision(const RECT& Walls);
    void Update(const Keyboard::State& Key, float DeltaTime);
    RECT GetRect() const;
    void Draw(const GeometryBatch& Batch);
    bool IsOverlappingWith(const RECT& Rect) override;

private:
    DirectX::XMVECTORF32 Color = DirectX::Colors::Wheat;
    float HalfWidth;
    float HalfHeight;
    float Speed = 500.0f;
    Vec2 Pos;

    VertexPositionColor V1;
    VertexPositionColor V2;
    VertexPositionColor V3;
    VertexPositionColor V4;

    float ExitXValue = 0.045f;
};
