//
// Game.cpp
//

#include "pch.h"
#include "Game.h"
#include "BrickManager.h"
#include "Ball.h"
extern void ExitGame() noexcept;

using namespace DirectX;
using namespace DirectX::SimpleMath;

using Microsoft::WRL::ComPtr;

Game::Game() noexcept(false) :
    MRetryAudio(false)
{
    MDeviceResources = std::make_unique<DX::DeviceResources>();
    MDeviceResources->RegisterDeviceNotify(this);
}

Game::~Game()
{
    if (MAudEngine)
    {
        MAudEngine->Suspend();
    }
}

// Initialize the Direct3D resources required to run.
void Game::Initialize(const HWND Window, const int Width, const int Height)
{
    MDeviceResources->SetWindow(Window, Width, Height);
    GameOver = true;
    MDeviceResources->CreateDeviceResources();
    CreateDeviceDependentResources();

    MDeviceResources->CreateWindowSizeDependentResources();
    CreateWindowSizeDependentResources();

    MKeyboard = std::make_unique<Keyboard>();
    MMouse = std::make_unique<Mouse>();
    MMouse->SetWindow(Window);

    AUDIO_ENGINE_FLAGS AudioFlags = AudioEngine_Default;
#ifdef _DEBUG
    AudioFlags |= AudioEngine_Debug;
#endif
    MAudEngine = std::make_unique<AudioEngine>(AudioFlags);

    MBrickExplSound = std::make_unique<SoundEffect>(MAudEngine.get(),
                                                     L"BrickExpl.wav");
    MGameOverSound = std::make_unique<SoundEffect>(MAudEngine.get(),
                                                    L"GameOver.wav");
    MPaddleHitSound = std::make_unique<SoundEffect>(MAudEngine.get(),
                                                     L"PaddleHit.wav");


    MyBall = new Ball(Vector2(500.0f, 500.0f), Vector2(-1.0f, -1.0f));
    Manager = std::make_unique<BrickManager>(std::move(MBrickExplSound));
    MyPaddle = new Paddle(Vector2(550.0f, 550.0f), 60.0, 10.0f);
    //Manager->CreateBricks(nBricksAcross, nBricksDown, brickWidth, brickHeight);
    Walls.left = 0.0f;
    Walls.top = 0.0f;
    Walls.right = Width;
    Walls.bottom = Height;
}

#pragma region Frame Update
// Executes the basic game loop.
void Game::Tick()
{
    MTimer.Tick([&]()
    {
        Update(MTimer);
    });

    Render();
}

// Updates the world.
void Game::Update(DX::StepTimer const& Timer)
{
    const float ElapsedTime = static_cast<float>(Timer.GetElapsedSeconds());

    if (MRetryAudio)
    {
        MRetryAudio = false;
        if (MAudEngine->Reset())
        {
            // TODO: restart any looped sounds here
        }
    }
    else if (!MAudEngine->Update())
    {
        if (MAudEngine->IsCriticalError())
        {
            MRetryAudio = true;
        }
    }

    const auto Kb = MKeyboard->GetState();
    if (Kb.Escape)
    {
        ExitGame();
    }
    if (Kb.Space && GameOver)
    {
        GameOver = false;
        Manager->RemoveAllBricks();
        Manager->CreateBricks(NBricksAcross, NBricksDown, BrickWidth, BrickHeight);
    }
    //paddle inputs
    MyPaddle->Update(Kb, ElapsedTime);

    if (!GameOver)
    {
        MyBall->Update(ElapsedTime);
    }
    if (MyBall->DoWallCollision(Walls) == 2)
    {
        GameOver = true;
        MGameOverSound->Play();
    }

    if (!GameOver)
    {
        MyBall->DoWallCollision(Walls);
        MyPaddle->DoWallCollision(Walls);
        if (MyPaddle->DoBallCollision(*MyBall))
        {
            if (!MPaddleHitSound->IsInUse())
            {
                MPaddleHitSound->Play();
            }
        }
    }
}
#pragma endregion

#pragma region Frame Render
// Draws the scene.
void Game::Render()
{
    // Don't try to render anything before the first Update.
    if (MTimer.GetFrameCount() == 0)
    {
        return;
    }

    Clear();

    MDeviceResources->PIXBeginEvent(L"Render");
    const auto Context = MDeviceResources->GetD3DDeviceContext();

    MSpriteBatch->Begin();

    auto Output = L"Level 0-1";

    if (GameOver)
    {
        Output = L"Press Space To Start";
    }

    MSpriteBatch->Draw(MBackground.Get(), MFullscreenRect);

    const Vector2 Origin = MFont->MeasureString(Output) / 2.f;
    MFont->DrawString(MSpriteBatch.get(), Output,
                       MFontPos, Colors::White, 0.f, Origin);

    MSpriteBatch->Draw(MBallTexture.Get(), MyBall->GetPosition(), nullptr,
                        Colors::White, 0.f, MOrigin, .03f);

    MSpriteBatch->End();

    if (!GameOver)
    {
        Context->OMSetBlendState(MStates->Opaque(), nullptr, 0xFFFFFFFF);
        Context->OMSetDepthStencilState(MStates->DepthNone(), 0);
        Context->RSSetState(MStates->CullNone());

        MEffect->Apply(Context);

        Context->IASetInputLayout(MInputLayout.Get());

        MBatch->Begin();
        MyPaddle->Draw(MBatch);
        Manager->UpdateBrickState(Manager->BrickList, *MyBall, MBatch);

        MBatch->End();
    }

    MDeviceResources->PIXEndEvent();

    // Show the new frame.
    MDeviceResources->Present();
}

// Helper method to clear the back buffers.
void Game::Clear()
{
    MDeviceResources->PIXBeginEvent(L"Clear");

    // Clear the views.
    const auto Context = MDeviceResources->GetD3DDeviceContext();
    const auto RenderTarget = MDeviceResources->GetRenderTargetView();
    const auto DepthStencil = MDeviceResources->GetDepthStencilView();

    Context->ClearRenderTargetView(RenderTarget, Colors::CornflowerBlue);
    Context->ClearDepthStencilView(DepthStencil, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
    Context->OMSetRenderTargets(1, &RenderTarget, DepthStencil);

    // Set the viewport.
    auto viewport = MDeviceResources->GetScreenViewport();
    Context->RSSetViewports(1, &viewport);

    MDeviceResources->PIXEndEvent();
}
#pragma endregion

#pragma region Message Handlers
// Message handlers
void Game::OnActivated()
{
    // TODO: Game is becoming active window.
}

void Game::OnDeactivated()
{
    // TODO: Game is becoming background window.
}

void Game::OnSuspending()
{
    MAudEngine->Suspend();
}

void Game::OnResuming()
{
    MTimer.ResetElapsedTime();

    MAudEngine->Resume();
}

void Game::OnWindowMoved()
{
    const auto OutputSize = MDeviceResources->GetOutputSize();
    MDeviceResources->WindowSizeChanged(OutputSize.right, OutputSize.bottom);
}

void Game::OnWindowSizeChanged(const int Width, const int Height)
{
    if (!MDeviceResources->WindowSizeChanged(Width, Height))
        return;

    CreateWindowSizeDependentResources();

    // TODO: Game window is being resized.
}

// Properties
void Game::GetDefaultSize(int& Width, int& Height) const noexcept
{
    // TODO: Change to desired default window size.
    Width = 800;
    Height = 600;
}
#pragma endregion

#pragma region Direct3D Resources
// These are the resources that depend on the device.
void Game::CreateDeviceDependentResources()
{
    auto Device = MDeviceResources->GetD3DDevice();
    auto Context = MDeviceResources->GetD3DDeviceContext();
    MStates = std::make_unique<CommonStates>(Device);
    MSpriteBatch = std::make_unique<SpriteBatch>(Context);
    MFont = std::make_unique<SpriteFont>(Device, L"myfile.spritefont");
    MEffect = std::make_unique<BasicEffect>(Device);
    MEffect->SetVertexColorEnabled(true);

    ComPtr<ID3D11Resource> resource;
    // TODO: Initialize device dependent objects here (independent of window size).
    DX::ThrowIfFailed(
        CreateWICTextureFromFile(Device, L"Background.png", nullptr,
                                 MBackground.ReleaseAndGetAddressOf()));
    DX::ThrowIfFailed(
        CreateInputLayoutFromEffect<VertexType>(Device, MEffect.get(),
                                                MInputLayout.ReleaseAndGetAddressOf()));

    DX::ThrowIfFailed(
        CreateWICTextureFromFile(Device, L"ball.png",
                                 resource.GetAddressOf(),
                                 MBallTexture.ReleaseAndGetAddressOf()));
    ComPtr<ID3D11Texture2D> BallTex;
    DX::ThrowIfFailed(resource.As(&BallTex));
    CD3D11_TEXTURE2D_DESC BallDesc;
    BallTex->GetDesc(&BallDesc);

    MOrigin.x = static_cast<float>(BallDesc.Width / 2);
    MOrigin.y = static_cast<float>(BallDesc.Height / 2);

    MBatch = std::make_unique<PrimitiveBatch<VertexType>>(Context);
}

// Allocate all memory resources that change on a window SizeChanged event.
void Game::CreateWindowSizeDependentResources()
{
    // TODO: Initialize windows-size dependent objects here.
    const auto size = MDeviceResources->GetOutputSize();
    const Matrix proj = Matrix::CreateScale(2.f / static_cast<float>(size.right),
                                            -2.f / static_cast<float>(size.bottom), 1.f)
        * Matrix::CreateTranslation(-1.f, 1.f, 0.f);
    MEffect->SetProjection(proj);
    MScreenPos.x = static_cast<float>(size.right) / 2.f;
    MScreenPos.y = static_cast<float>(size.bottom) / 10.f;

    MStretchRect.left = size.right / 4;
    MStretchRect.top = size.bottom / 4;
    MStretchRect.right = MStretchRect.left + size.right / 2;
    MStretchRect.bottom = MStretchRect.top + size.bottom / 2;

    MFullscreenRect = MDeviceResources->GetOutputSize();

    MFontPos.x = static_cast<float>(size.right) / 2.f;
    MFontPos.y = static_cast<float>(size.bottom) / 2.f;

    if (Manager)
    {
        Manager->SetBrickSpawnOffset(MFontPos.x, MFontPos.y);
    }
}

void Game::OnDeviceLost()
{
    // TODO: Add Direct3D resource cleanup here.
    MSpriteBatch.reset();
    MStates.reset();
    MBackground.Reset();
    MFont.reset();
    MEffect.reset();
    MBatch.reset();
    MInputLayout.Reset();
    MBallTexture.Reset();
}

void Game::OnDeviceRestored()
{
    CreateDeviceDependentResources();

    CreateWindowSizeDependentResources();
}
#pragma endregion
